import React from "react";

function Footer(){

    return (
        <div className={"footer-container"}>
            <div className="footer-wrapper">
                <div className="brief-about">
                    <div className="logo-container">
                        <img src="./images/frutiv-logo.png" alt=""/>
                    </div>

                </div>
            </div>
        </div>
    )
}
