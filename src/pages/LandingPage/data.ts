export const drive = [
  {
    name: "clarity:email-line",
    label: "Email Marketing",
  },
  {
    name: "nimbus:marketing",
    label: "Social Media Marketing",
  },
  {
    name: "clarity:video-gallery-line",
    label: "Video Marketing",
  },
  {
    name: "carbon:phone-application",
    label: "Leading Recovery",
  },
  {
    name: "icon-park-outline:seo",
    label: "Search Engine Optimization",
  },
];

export const topServices = [
  {
    image: "/web.png",
    title: "Web Development",
    text: "You really need a well structures website that can  make your audience ",
    description: "",
  },
  {
    image: "/digital.png",
    title: "Digital Marketing",
    text: "We get traffic to your site with our experience in internet word, and user preferences",
    description: "",
  },
  {
    image: "/mobile.png",
    title: "Mobile App Development",
    text: "Want to satisfy your user with the look and interface of your site?, frutiv team gat you with the best",
    description: "",
  },
  {
    image: "/ui-ux.png",
    title: "UI/UX Designs",
    text:
      "User Interface Design and User Experience play a key part in retaining users by making it convenient" +
      " to use the product and by providing customer delight.",
    desscription: "",
  },
  {
    image: "/branding.png",
    title: "Branding",
    text: "Let get your blockchain financing on the go, making you experience the world of cryptocurrency.",
    desscription: "",
  },
  {
    image: "/blockchain.png",
    title: "Blockchain Development",
    text: "Let get your blockchain financing on the go, making you experience the world of cryptocurrency.",
    desscription: "",
  },
];
